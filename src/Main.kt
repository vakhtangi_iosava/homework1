fun main(){

    var age: Byte = 25
    var character = 'p' //Char
    var s: Short = 100
    var number = 1000 //Int
    var l: Long = 10000 //es udris l = 10000L

    val pi: Float = 3.14F
    val d = 9.054 //Double

    val b: Boolean = false
//
//    if(age<10){
//        println("cant")
//    }else if (age < 14){
//        println("can with")
//    } else{
//        println("can")
//    }
//
//    for (i in 3..20 step 3){
//        println(i)
//    }

    sayHello("jemali")
}

fun sayHello(name: String){
    println("Hello, $name")
}